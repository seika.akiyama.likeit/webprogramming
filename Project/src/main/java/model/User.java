package model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Userテーブルのデータを格納するためのBeans
 *
 * @author takano
 */
public class User implements Serializable {
  private int id;
  private String loginId;
  private String name;
  private Date birthDate;
  private String password;
  private boolean isAdmin;
  private Timestamp createDate;
  private Timestamp updateDate;



  public User() {}

  // ☆ログインセッションを保存するためのコンストラクタ
  public User(String loginId, String name) {
    this.loginId = loginId;
    this.name = name;
  }

  // 全てのデータをセットするコンストラクタ
  public User(int id, String loginId, String name, Date birthDate, String password, boolean isAdmin,
      Timestamp createDate, Timestamp updateDate) {
    super();
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
    this.isAdmin = isAdmin;
    this.createDate = createDate;
    this.updateDate = updateDate;
  }

  public User(String loginIdData) {
    this.loginId = loginId;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean getIsAdmin() {
    return isAdmin;
  }

  public void setIsAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  public Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public Timestamp getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Timestamp updateDate) {
    this.updateDate = updateDate;
  }



}