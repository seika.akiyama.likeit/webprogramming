package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 * ユーザテーブル用のDao
 *
 * @author takano
 */
public class UserDao {

  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す S
   * 
   * @param loginId
   * @param password
   * @return
   */
  public User findByLoginInfo(String login_id, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, login_id);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      return new User(loginIdData, nameData);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  /**
   * 全てのユーザ情報を取得する
   *
   * @return
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM user WHERE is_admin=0;";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  // 検索
  public List<User> search(String login_id, String username, String startbirthdate,
      String endbirthdate) {
    Connection conn = null;
    
    ArrayList<User> userList = new ArrayList<User>();
    ArrayList<String> strList = new ArrayList<String>();
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      
      
      // 基準
      String sql =
          "SELECT * FROM user WHERE is_admin=0";
   
      // login_id =? AND name=? AND birth_date=?  AND birth_date=?
      StringBuilder StringBuilder = new StringBuilder(sql);
      
      if(!login_id.equals("")) {
        StringBuilder.append(" AND login_id = ?"); // そのままつながるからANDの前 スペース！
      strList.add(login_id);
      }
      if (!username.equals("")) {
        StringBuilder.append(" AND name LIKE ?");
        strList.add("%" + username + "%");
      }
      if (!endbirthdate.equals("")) {
        StringBuilder.append(" AND ?>= birth_date");
        strList.add(endbirthdate);
      }
      if(!startbirthdate.equals("")) {
        StringBuilder.append(" AND ?<= birth_date");
        strList.add(startbirthdate);
      }
      
      PreparedStatement pStmt = conn.prepareStatement(StringBuilder.toString());
      for(int i=0;i < strList.size();i++) {
        pStmt.setString(i+1, strList.get(i));
      }
     
      ResultSet rs = pStmt.executeQuery(); 
        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        // if (!rs.next()) {
        // return null;
        // }//結果必ずいっこのときだけ

      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
      }catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
    }
  // Userに新規登録
  public void userAdd(String id, String password, String username,
      String birthdate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) "
              + "VALUES (?,?,?,?,now(),now())";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      pStmt.setString(2, username);
      pStmt.setString(3, birthdate);
      pStmt.setString(4, password);
      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }

  // userIDあるか検索するDao
  public User findUserId(String login_id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id =?;";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, login_id);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id");
      return new User(loginIdData);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



  // User詳細
  public User userDetail(String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "SELECT *  FROM user WHERE id =?;";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータじゃないやつもインスタンスのフィールドに追加
      int idData = rs.getInt("id");
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String passwordDate = rs.getString("password");
      boolean isAdminData = rs.getBoolean("is_admin");
      Timestamp createDateData = rs.getTimestamp("create_date");
      Timestamp updateDateData = rs.getTimestamp("update_date");

      return new User(idData, loginIdData, nameData, birthDate, passwordDate, isAdminData,
          createDateData, updateDateData);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



  // User更新 パスワードあり
  public void userUpdatePass(String username, String birthdate, String password, String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "UPDATE user SET name =?, birth_date =?, password = ?,update_date = now() WHERE id =? ;";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, username);
      pStmt.setString(2, birthdate);
      pStmt.setString(3, password);
      pStmt.setString(4, id);
      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }

  // User更新 パスワードなし
  public void userUpdate(String username, String birthdate, String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "UPDATE user SET name = ? , birth_date =? ,update_date = now() WHERE id =? ;";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, username);
      pStmt.setString(2, birthdate);
      pStmt.setString(3, id);
      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }
  // User削除
  public void userDelete(String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "DELETE FROM user WHERE id = ?;";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }
}


