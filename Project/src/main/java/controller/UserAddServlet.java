package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Util.PasswordEncorder;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      User user = (User) session.getAttribute("userInfo");

      if (user == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("user-loginid");
      String password = request.getParameter("password");
      String passwordc = request.getParameter("password-confirm");
      String username = request.getParameter("user-name");
      String birthdate = request.getParameter("birth-date");

      User user = new User();
      // Date sqlDate = Date.valueOf(birthdate);
      // 空白の時できないから５００のエラー
      // Addだったらいらないrequest.setAttribute("loginId", loginId);でできてる

      if (!birthdate.equals("")) {
        Date sqlDate = Date.valueOf(birthdate); //


        user.setBirthDate(sqlDate); //
      }


      UserDao userDao = new UserDao();
      if (loginId.equals("") || password.equals("") || passwordc.equals("") || username.equals("")
          || birthdate.equals("")) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        // キーで置いてあげる(塊で渡してあげる)
        request.setAttribute("user", user);

        request.setAttribute("loginId", loginId);
        request.setAttribute("name", username);
        request.setAttribute("birth-date", birthdate);
        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }
      if (userDao.findUserId(loginId) != null) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        // 入力したログインIDを画面に表示するために値をセット
        request.setAttribute("loginId", loginId);

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }


      String password2 = PasswordEncorder.encordPassword(password);
      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行

      userDao.userAdd(loginId, password2, username, birthdate);


      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");
}
}
