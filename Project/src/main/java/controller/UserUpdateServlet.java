package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Util.PasswordEncorder;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      User user2 = (User) session.getAttribute("userInfo");

      if (user2 == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      // (はてなの後ろ URLみるとID=1とかなってる)
      String Id = request.getParameter("id");

      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      UserDao userDao = new UserDao();
      User user = userDao.userDetail(Id);

      // キーで置いてあげる(塊で渡してあげる)
      request.setAttribute("user", user);
      
    
 


      // ユーザ詳細のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // getパラメーター
      // if（空白）か両方パスあるか
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得 //JSPとサーブレット
      String id = request.getParameter("user-id");
      String password = request.getParameter("password");
      String passwordc = request.getParameter("password-confirm");
      String username = request.getParameter("user-name");
      String birthdate = request.getParameter("birth-date");
      String loginId = request.getParameter("user-loginId");

      User user = new User();
      int num = Integer.parseInt(id);

      user.setId(num); //
      user.setLoginId(loginId);
      user.setName(username);
      if (!birthdate.equals("")) {
        Date sqlDate = Date.valueOf(birthdate); //


      user.setBirthDate(sqlDate); //
      }


      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      UserDao userDao = new UserDao();
      if (username.equals("") || birthdate.equals("")) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        // キーで置いてあげる(塊で渡してあげる)
        request.setAttribute("user", user);


        // ログインjspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }
      if (!password.equals(passwordc)) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        // キーで置いてあげる(塊で渡してあげる)
        request.setAttribute("user", user);
        // ログインjspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }
      String password2 = PasswordEncorder.encordPassword(password);
      if (password.equals("") || passwordc.equals("")) {
        userDao.userUpdate(username, birthdate, id);
      } else {
        userDao.userUpdatePass(username, birthdate, password2, id);
      }

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");
    }


	}


