package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDelete
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      User user2 = (User) session.getAttribute("userInfo");

      if (user2 == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      request.setCharacterEncoding("UTF-8");
      // (はてなの後ろ URLみるとID=1とかなってる)
      String Id = request.getParameter("id");

      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      UserDao userDao = new UserDao();
      User user = userDao.userDetail(Id);


      // キーで置いてあげる(塊で渡してあげる)
      request.setAttribute("user", user);

      // ユーザ詳細のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
      dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      // リクエストパラメータの入力項目を取得 //JSPとサーブレット
      String id = request.getParameter("user-id");


      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      UserDao userDao = new UserDao();
      userDao.userDelete(id);

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");
    }



}
