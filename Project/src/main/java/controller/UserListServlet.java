package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      User user = (User) session.getAttribute("userInfo");

      if (user == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
	  // ユーザ一覧情報を取得
      UserDao userDao = new UserDao();
      List<User> userList = userDao.findAll();

      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("userList", userList);
      // ユーザ一覧のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("user-loginid");
      String username = request.getParameter("user-name");
      String datestart = request.getParameter("date-start"); // 値を貰う
      String dateend = request.getParameter("date-end");


      UserDao userDao = new UserDao();
      List<User> userList = userDao.search(loginId, username, datestart, dateend);// 詰める// daoに飛ぶ



      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("userList", userList);

      // TODO 未実装：検索処理全般

      // TODO 未実装：どんな条件で検索したかわかるように画面に入力値を表示させること
      // ユーザ一覧のサーブレットにリダイレクト
      // ユーザ一覧のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
	}

}
